package com.safebear.app;

import org.junit.Test;

import static org.junit.Assert.assertTrue;

/**
 * Created by CCA_Student on 20/11/2017.
 */
public class Test01_Login extends BaseTest {
    @Test
    public void testLogin() {
        //Step 1 Confirm we're on the Welcome Page
        assertTrue(welcomePage.checkCorrectPage());

        //Step 2 Click on the Login link and the Login Page loads
        assertTrue(welcomePage.clickOnLogin(this.loginPage));

        //Step 3 Login with valid credentials
        assertTrue(loginPage.login(this.userPage,"testuser","testing"));

        //Step 4 Click on the Logout link and the Welcome Page loads
        assertTrue(userPage.clickOnLogout(this.welcomePage));

    }
}
